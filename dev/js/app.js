$(document).ready(function(){

  $(document).foundation();

  $('.testimonials-list').slick({
    dots: true
  });

  setTimeout(function () {
      var items = $('.paused'), i = 0, max = items.length;
      var doTimeout = function () {
          if (i < max) {
              items.eq(i).removeClass("paused");
              i++;
              setTimeout(doTimeout, 200);
          }
      };
      doTimeout();
  }, 1000);

  setTimeout(function () {
      var items = $('.paused-2'), i = 0, max = items.length;
      var doTimeout = function () {
          if (i < max) {
              items.eq(i).removeClass("paused-2");
              i++;
              setTimeout(doTimeout, 200);
          }
      };

      doTimeout();
  }, 2000);

  $(".tabs-content > .content").css("opacity","0");
  $(".tabs-content > .content.active").css("opacity","1");
  //$(".content:first-child h2, .content:first-child p").css("opacity",1);

  $(".tab-title").click(function(){
    $(".tabs-content > .content").removeClass("animated fadeInRight");
    $(".content h2, .content p").removeClass("animated fadeInDown");

    setTimeout(function () {
      $(".tabs-content > .content, .content h2, .content p").css("opacity","0");
      $(".tabs-content > .content.active").addClass("animated fadeInRight");
      setTimeout(function () {
        $(".content.active h2, .content.active p").addClass("animated fadeInDown");
      }, 1000);
    }, 100);
  });

});

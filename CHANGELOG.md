# ProGás Changelog

### 1.8.1 (9 de Maio de 2015)

* Menu com sombra;
* Novas imagens;

### 1.8.0 (21 de Abril de 2015)

* Menu fixo;
* Corrigido a imagem da página de produtos;

### 1.7.0 (17 de Abril de 2015)

* Corrigido bugs do menu e do menu lateral (Firefox);
* Colocado as imagens de apresentação;
* Melhorado a animação das características da Home e o efeito na tabela de preços;

### 1.6.0 (12 de Abril de 2015)

* Adicionado as animações nas páginas;
* Adicionado os textos e links das redes sociais e whatsapp, além de incluir no rodapé.;

### 1.5.0 (16 de Março de 2015)

* Otimizado CSS removendo seletores inutilizados: 259.93 kb -> 69.59 kb (style.clean.css);
* Adicionado favicon;

### 1.4.0 (15 de Março de 2015)

* Otimizado para mobile;

### 1.3.0 (11 de Março de 2015)

* Página de Blog e Serviços;

### 1.2.0 (10 de Março de 2015)

* Ícones na home e ajuda;
* Página de ajuda;

### 1.1.0 (4 de Março de 2015)

* Modularização do template;
* Página de produto e preço;

### 1.0.0 (2 de Março de 2015)

* Configurado com Grunt, Bourbon, Foundation;
* Criado a Home;
